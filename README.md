Oracle SOA Suite on Docker
==============================
Sample Docker build file to facilitate installation, configuration, and environment setup for DevOps users for Releases 12.2.1.3.0. For 
[Oracle Fusion Middleware](http://www.oracle.com/technetwork/middleware/fusion-middleware/overview/index.html) specifically the sections on
- Service Integration (SOA)
- Business Process Management

## add swap file if not exist 
```buildoutcfg
sudo fallocate -l 2G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
sudo free -h
```
## How to build
1  - database image to be used 
```buildoutcfg
cd Database
./buildDockerImage.sh -v 12.2.0.1 -e
```

2 - build Java8  to be used on FMWInfrastructure
```buildoutcfg
cd ../Java8
docker build -t oracle/serverjre:8 .
```

3 - build FMWInfrastructure to be used on SOA Suite

```buildoutcfg
cd ../FMWInfrastructure_12.2.1.3/
docker build -f Dockerfile -t oracle/fmw-infrastructure:12.2.1.3 .

```
4 - build  SOA Suite
```buildoutcfg
cd ../SOASuite_12.2.1.3/
docker build -t oracle/soa:12.2.1.3 .
```

## How to run

 
 1 - create databse container: note that ORACLE_PWD is a combined password and does not start with number
```

docker run -d --name soadb -p 1521:1521 -p 5500:5500 -e ORACLE_SID=soadb -e ORACLE_PDB=soapdb \
-e ORACLE_PWD=mqBoBx4pass_  oracle/database:12.2.0.1-ee

docker logs -f soadb
```
- wait logs to be
```
------------------
The Oracle base remains unchanged with value /opt/oracle
#########################
DATABASE IS READY TO USE!
#########################
The following output is no
------------------------
```

2 -create soaa admin server 
```
docker run -d -p 7001:7001 --name soaas \
-e DOMAIN_TYPE=soa \
-e ADMIN_HOST=35.222.45.34 \
-e ADMIN_PASSWORD=mqBoBx4pass_ \
-e CONNECTION_STRING=35.222.45.34:1521/soapdb \
-e DB_PASSWORD=mqBoBx4pass_ \
-e DB_SCHEMA_PASSWORD=mqBoBx4pass_ \
-e RCUPREFIX=SOA01 \
-e MANAGED_SERVER=soa_server1 \
oracle/soa:12.2.1.3 \
/bin/bash -c "sleep 5s; /u01/oracle/container-scripts/createDomainAndStart.sh"

docker logs -f soasa

```
 - wait till logs

 ```
 INFO: Domain updated successfully
INFO: Updating the listen address - 172.17.0.3  35.222.45.34
/u01/oracle/oracle_common/common/bin/wlst.sh -skipWLSModuleScanning /u01/oracle/container-scripts/updListenAddress.py u01 172.17.0.3 AdminServer 35.222.45.34
/u01/oracle/user_projects/domains/infra_domain/soa/oracle.soa.ext_11.1.1 does not exist... creating it now...
INFO: Starting the Admin Server...
INFO: Logs = /u01/oracle/user_projects/domains/infra_domain/logs/as.log
 ```

 - now you can access admin server 
 ```
 http://35.222.45.34:7001/console
 http://35.222.45.34:7001/em
```
access using  weblogic/mqBoBx4pass_

#### reference 

https://github.com/oracle/docker-images/tree/master/OracleSOASuite